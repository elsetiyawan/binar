import "@/styles/globals.css";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";

export default function App({ Component, pageProps }) {
  const [cookie] = useCookies(["loginData"]);
  const router = useRouter();

  useEffect(() => {
    if (!cookie.loginData) {
      router.push("/login");
    }
  });

  return <Component {...pageProps} />;
}
