import React, { useEffect, useState } from "react";

export default function GameHistory() {
  const [history, setHistory] = useState([]);

  useEffect(() => {
    const data = localStorage.getItem("winningHistory");
    const parsedData = JSON.parse(data);
    setHistory(parsedData.history);
  }, []);

  useEffect(() => {
    console.log(history);
  }, [history]);

  const format_time = (s) => {
    const dtFormat = new Intl.DateTimeFormat("en-GB", {
      timeStyle: "medium",
      timeZone: "UTC",
    });

    return dtFormat.format(new Date(s * 1e3));
  };
  return (
    <div style={{ padding: "20px" }}>
      {history.map((x) => (
        <div style={{ display: "flex" }} key={x.playDate}>
          <div style={{ padding: "10px" }}>{format_time(x.playDate)}</div>
          <div style={{ padding: "10px" }}>{x.status}</div>
        </div>
      ))}
    </div>
  );
}
