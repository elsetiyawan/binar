import Image from "next/image";
import batu from "../../public/images/batu.png";
import gunting from "../../public/images/gunting.png";
import kertas from "../../public/images/kertas.png";
import { useState } from "react";
import { useEffect } from "react";
import { useCookies } from "react-cookie";

export default function Game() {
  const [userChoice, setUserChoice] = useState();
  const [comChoice, setComChoice] = useState();
  const [winner, setWinner] = useState();
  const [cookie] = useCookies(["loginData"]);
  const [user, setUser] = useState({});

  const handleClick = (choice) => {
    if (!userChoice) {
      setUserChoice(choice);
      comChoose();
    }
  };

  const comChoose = () => {
    const choices = ["comRock", "comPaper", "comScissor"];
    const rndInt = Math.floor(Math.random() * 3);
    const choosen = choices[rndInt];
    setComChoice(choosen);
  };

  useEffect(() => {
    setUser(cookie.loginData);
  }, [cookie.loginData]);

  useEffect(() => {
    if (userChoice !== undefined && comChoice !== undefined) {
      if (
        (userChoice === "userRock" && comChoice === "comRock") ||
        (userChoice === "userPaper" && comChoice === "comPaper") ||
        (userChoice === "userScissor" && comChoice === "comScissor")
      ) {
        announceWinner("draw");
      } else if (
        (userChoice === "userRock" && comChoice === "comScissor") ||
        (userChoice === "userPaper" && comChoice === "comRock") ||
        (userChoice === "userScissor" && comChoice === "comPaper")
      ) {
        announceWinner("user");
      } else {
        announceWinner("com");
      }
    }
  }, [userChoice, comChoice]);

  const announceWinner = (status) => {
    const data = localStorage.getItem("winningHistory");
    const newData = { playDate: Date.now(), status: status };
    if (data) {
      const parsedData = JSON.parse(data);
      localStorage.setItem(
        "winningHistory",
        JSON.stringify({
          username: parsedData.username,
          history: [ ...parsedData.history, newData ],
        })
      );
    } else {
      const recordNew = { username: user.username, history: [newData] };
      console.log(recordNew);
      localStorage.setItem("winningHistory", JSON.stringify(recordNew));
    }
    setWinner(status);
  };

  return (
    <div style={{ padding: "20px", height: "100vh" }}>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: userChoice === "userRock" ? "red" : "",
            }}
            onClick={() => {
              handleClick("userRock");
            }}
          >
            <Image priority alt="image" src={batu} width={200} height={200} />
          </div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: userChoice === "userScissor" ? "red" : "",
            }}
            onClick={() => {
              handleClick("userScissor");
            }}
          >
            <Image
              priority
              alt="image"
              src={gunting}
              width={200}
              height={200}
            />
          </div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: userChoice === "userPaper" ? "red" : "",
            }}
            onClick={() => {
              handleClick("userPaper");
            }}
          >
            <Image priority alt="image" src={kertas} width={200} height={200} />
          </div>
        </div>
        <div>
          <div>VS</div>
          <div>{winner ?? ""}</div>
          <div
            onClick={() => {
              console.log("hei there");
              setComChoice(undefined);
              setUserChoice(undefined);
              setWinner(undefined);
            }}
            style={{
              cursor: "pointer",
            }}
          >
            Reset
          </div>
        </div>
        <div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: comChoice === "comRock" ? "red" : "",
            }}
          >
            <Image priority alt="image" src={batu} width={200} height={200} />
          </div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: comChoice === "comScissor" ? "red" : "",
            }}
          >
            <Image
              priority
              alt="image"
              src={gunting}
              width={200}
              height={200}
            />
          </div>
          <div
            style={{
              padding: "20px",
              borderRadius: "15px",
              marginBottom: "20px",
              cursor: "pointer",
              backgroundColor: comChoice === "comPaper" ? "red" : "",
            }}
          >
            <Image priority alt="image" src={kertas} width={200} height={200} />
          </div>
        </div>
      </div>
    </div>
  );
}
