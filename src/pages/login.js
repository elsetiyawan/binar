import { useRouter } from "next/router";
import React from "react";
import { useState } from "react";
import { useCookies } from "react-cookie";

export default function Login() {
  const [loginData, setLoginData] = useState({ username: "", password: "" });
  const [cookie, setCookie] = useCookies(["loginData", "userData"]);
  const router = useRouter();
  const handleClick = () => {
    if (loginData.username !== "" && loginData.password !== "") {
      setCookie("loginData", loginData, { maxAge: 6000 });
      setCookie(
        "userData",
        {
          username: "Username is here",
          fullname: "The Fullname is here",
          phoneNumber: "Phone numbe is here",
          email: "Email is here",
        },
        { maxAge: 6000 }
      );
      router.push("/dashboard");
    }
  };
  return (
    <div style={{ padding: "20px" }}>
      <div>
        <input
          placeholder="username"
          onChange={(e) => {
            e.preventDefault();
            setLoginData({ ...loginData, username: e.target.value });
          }}
        />
        <input
          placeholder="password"
          onChange={(e) => {
            e.preventDefault();
            setLoginData({ ...loginData, password: e.target.value });
          }}
        />
        <button onClick={handleClick}>Login</button>
      </div>
    </div>
  );
}
