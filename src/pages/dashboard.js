import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";

export default function Dashboard() {
  const [cookie] = useCookies(["userData"]);
  const [userData, setUserData] = useState({});
  useEffect(() => {
    setUserData(cookie.userData);
  });

  return (
    <div style={{ display: "flex", padding: "20px", height: "100vh" }}>
      <div style={{ flexGrow: 1, display: "flex" }}>
        <div>
          <Link href="/game" style={{ textDecoration: "none" }}>
            <div
              style={{
                textDecoration: "none",
                padding: "50px",
                border: "1px solid green",
                cursor: "pointer",
                backgroundColor: "red",
                width: "300px",
              }}
            >
              <center>ME VS Comp</center>
            </div>
          </Link>
        </div>
      </div>
      <div
        style={{
          flexGrow: 1,
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          backgroundColor: "lightcyan",
        }}
      >
        <div>Avatar</div>
        <div>{userData.fullname}</div>
        <div>{userData.username}</div>
        <div>{userData.email}</div>
        <div>{userData.phonenumber}</div>
        <div>Game History</div>
      </div>
    </div>
  );
}
